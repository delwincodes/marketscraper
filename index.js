'use strict';

const request = require('request');

const TODAY = new Date().getTime()/1000;
/** EDIT THIS TO FETCH DATA FOR MORE/DIFFERENT EXCHANGES **/
// leave as empty array to get all exchanges
const EXCHANGES = ['BTRX', 'BINA', 'PLNX', 'LIQU', 'YOBT', 'HITB'];
//const EXCHANGES = [];

var total;
//const START = TODAY-(60*60*24*365*3); // grab 3 years of data if possible
const START = TODAY - 60*60*3;

class Market {
  constructor(params) {
    this.exchange = params.exchange;
    this.coin = params.coin;
    this.base = params.base;
  }

  toString() {
    return `${this.exchange}:${this.coin}/${this.base}`;
  }
}

function getHistoryUrl(market, time = 60) {
  return `https://www.coinigy.com/getjson/chart_feed/${market.exchange}/${market.coin}/${market.base}/${time}/${START}/${TODAY}`;
}

function saveCandles(market, interval, data, cb) {
  let candles = [];
  for (var i = 0; i < data.length; i++) {
    candles.push({
      start: data[i][0],
      open: data[i][1],
      high: data[i][2],
      low: data[i][3],
      close: data[i][4],
      exchange: market.exchange,
      coin: market.coin,
      base: market.base
    });
  }
  // bulk write candles to db
  // right here
  //console.log(candles);
  cb();
}

function getHistory(market, time = 60, cb = () => console.log('Done.')) {
  console.log(`Getting ${time}m candles for ${market.toString()}`);
  let url = getHistoryUrl(market, time);
  request(url, (err, res, body) => {
    if (!err && res.statusCode === 200) {
      saveCandles(market, time, body, cb);
    } else {
      console.error(`Error getting candle data for ${market.toString()}`);
      console.error(err);
    }
  });
}

function getMarkets(cb) {
  console.log('Fetching market list...');
  request('https://www.coinigy.com/getjson/getmarkets', (err, res, body) => {
    if (!err && res.statusCode === 200) {
      let markets = [];
      console.log(`Found ${JSON.parse(body).length} markets.`);
      JSON.parse(body).forEach(m => {
        if (!EXCHANGES.length || EXCHANGES.includes(m.exch_code)) {
          markets.push(new Market({
            coin: m.primary_curr_code,
            base: m.secondary_curr_code,
            exchange: m.exch_code
          }));
        }
      });
      cb(null, markets);
    } else {
      console.error('Error getting markets:');
      console.error(err);
      cb(err);
    }
  });
}

function scrape (markets) {
  if (markets.length) {
    console.log(`${((1-markets.length/total)*100).toFixed(2)}% done`);
    getHistory(markets.pop(), 60, () => scrape(markets));
  } else {
    console.log('Done.');
  }
}

// go
getMarkets((err, markets) => {
  if (!err && markets && markets.length) {
    total = markets.length;
    console.log(`Scraping candle data for ${markets.length} markets.`);
    scrape(markets);
  }
});