Scrapes coinigy API for hourly candle data.

## Quickstart

Requires Node & NPM.

1. Run `npm i` to get dependencies.
2. Edit `index.js` to insert into your DB or alter the exchanges queried.
3. Run `node index`